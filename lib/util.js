const Discord = require("discord.js")

const responseTypeIsPicture = (response) => {
  return (/.(gif|png|jpg|jpeg)/).test(response)
}

const isValidResponse = (response) => {
  return response != null && response != ""
}

const handleResponse = (message, response) => {
  if(responseTypeIsPicture(response)) {
    message.channel.send(new Discord.Attachment(response, response))
  } else {
    message.channel.send(response)
  }
}

const blockText = (message) => {
  return `\`\`\`${message}\`\`\``
}

module.exports =  {
  isValidResponse,
  handleResponse,
  blockText
}