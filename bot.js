const fs = require("fs")
const Discord = require("discord.js")
const logger = require("winston")
const { token } = require("./auth/auth.json")

const util = require("./lib/util.js")
const simpleCommandDictionary = require("./src/simple-command-dictionary")
const complexCommandDictionary = require("./src/complex-command-dictionary.js")

const loadCommands = () => {
  // Not used yet.
  const commandFiles = fs.readdirSync("./src")
  for (const file of commandFiles) {
    const command = require(`./src/${file}`)
    bot.commands.set(command.name, command)
  }
}

const initLogger = () => {
  logger.remove(logger.transports.Console)
  logger.add(logger.transports.Console, {
      colorize: true
  })
  logger.level = 'debug'
}

/*
  Initialization of Discord Bot
*/
const bot = new Discord.Client()
bot.commands = new Discord.Collection()
initLogger()

bot.on('ready', () => {
    logger.info('Connected')
    logger.info('Logged in as: ')
    logger.info(bot.user.username + ' - (' + bot.user.id + ')')
})

bot.on('message', (message) => {
  if (message.author.bot) return
  let response = null

  response = simpleCommandDictionary[message.content]
  if (util.isValidResponse(response)) {
    util.handleResponse(message, response)
    return
  }

  response = complexCommandDictionary(message.author, message.content)
  if(util.isValidResponse(response)) {
    util.handleResponse(message, response)
    return
  }
})

bot.login(token)
