const util = require("../lib/util.js")

const totalBestGirls = 5

const regexDictionary = {
  "(doge say)"                              :     (user, chatText) => { return say(user, chatText) },
  "(doge)(.*)(roll for me)"                 :     (user, chatText) => { return roll(user, chatText) },
  "(doge)(.*)(pick from)"                   :     (user, chatText) => { return pickFrom(user, chatText) },
  "(doge xkcd)"                             :     (user, chatText) => { return findReleventXkcd(user, chatText) },
  "(can\'t|cant).*(if)"                     :     (user, chatText) => { return thatsThinking(user, chatText) },
  "(doge help)"                             :     (user, chatText) => { return help(user, chatText) },
  "(doge)(.*)(who)(.*)(best girl)"          :     (user, chatText) => { return bestGirl(user, chatText) },
  "(best girl)"                             :     (user, chatText) => { return specificBestGirl(user, chatText)},
  "(doge)(honor the fallen)"                :     (user, chatText) => { return honorTheFallen(user, chatText)}
}

const getAsset = (assetName) => {
  return "assets/" + assetName
}

const findReleventXkcd = (user, chatText) => {
  let num = Math.floor((Math.random() * 1800) + 1)
  let url = 'http://xkcd.com/' + num
  return 'Is this a relevent XKCD: ' + url + ' ?'
}

const roll = (user, chatText) => {
  let generatedNumber = Math.floor((Math.random() * 100) + 1)
  return util.blockText(user + " has rolled a " + generatedNumber + ".")
}

const say = (user, chatText) => {
  let regex = new RegExp('(doge say)(\\s)(.*)', 'i')
  return util.blockText(chatText.match(regex)[3])
}

const pickFrom = (user, chatText) => {
  let regex = new RegExp("[`~!@#$%^&*()_|+\-=?:'\",.<>\{\}\[\]\\\/]", "ig")
  try{
    let choiceList = chatText.match(/(doge.*pick.*from )(.*)/i)[2].split(/[,]+/)
    let choiceCount = choiceList.length
    return util.blockText("wow, very " + choiceList[Math.floor(Math.random() * choiceCount)].trim().replace(regex) + ", much delicious.")
  }
  catch(ex){
    logger.info(ex)
    return util.blockText("Wow, very invalid. Much empty. So list.")
  }
}

const thatsThinking = (user, chatText) => {
  let regex = /((^|\s)can't\s|^cant\s).*if/i
  if(regex.test(chatText)){
      return assetPath + "thatsthinking.jpg"
  }
}

const bestGirl = (user, chatText) => {
  return getAsset("kaede" + Math.floor((Math.random() * totalBestGirls) + 1) + ".png")
}

const specificBestGirl = (user, chatText) => {
  if(user.username == "( ´ ▽ ` )ﾉ"){
    return getAsset("kirari1.jpg")
  }
  else if(user.username == "Yuu"){
    return getAsset("kaede" + Math.floor((Math.random() * totalBestGirls) + 1) + ".png")
  }
  else {
    return util.blockText(`I has no waifu for you, berk. Such unfortunate. So have this message instead. \o/`)
  }
}

const honorTheFallen = (user, chatText) => {
  const exBorkers = [
    "The Micchi",
    "Robbo",
    "Paul Baker",
    "Peter Da Nickel-a",
    "Alex oWolff",
    "Natalie the Intern",
    "Muh-rie",
    "Stliphen",
    "Airyck"
  ]

  const fullString = exBorkers
                      .map( (bork) => bork + "\n")
                      .join("")
  
  return util.blockText(fullString)
}

module.exports = (user, chatText) => {
  for(let regex in regexDictionary) {
    let regexp = new RegExp(regex, "igm")
    if(chatText.match(regexp)) {
      return regexDictionary[regex](user, chatText)
    }
  }
}