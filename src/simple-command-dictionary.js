const getAsset = (assetName) => {
  return "assets/" + assetName
}

module.exports = {
  "howaboutno"            :   getAsset("howaboutno.gif"),
  "dogeintensifies"       :   getAsset("dogeintensifies.gif"),
  "gitgud"                :   getAsset("gitgud.png"),
  "swine"                 :   getAsset("swine.gif"),
  "whatintarnation"       :   getAsset("whatintarnation.jpg"),
  "thisisfine"            :   getAsset("thisisfine.png"),
  "hesright"              :   getAsset("hesrightyouknow.jpg"),
  "shesright"             :   getAsset("shesrightyouknow.jpg"),
  "cultured"              :   getAsset("cultured.jpg"),
  "tidus"                 :   getAsset("tidus.gif"),
  "unittest"              :   getAsset("thisisfine.png"),
  "thonking"              :   getAsset("thonking.jpg")
}
