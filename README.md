# Doge Bot - Discord Edition
Continuation of the Doge Bot that was created as part of Rocket.Chat in a certain place, somewhere out there.

The project is built off of [discord.js](https://discord.js.org/#/) and requires [node.js](https://nodejs.org/en/) in order to compile and run the application.

## Installation
Clone the the project in a directory of your choice by running the following:
```
git clone https://gitlab.com/LiefEriccson/DiscordDogeBot.git
```

Once the cloning is complete, run the following to get the packages needed by the project:
```
npm install
```

If you're having issues installing npm/node on Windows, that's because Windows sucks for development. But you can thankfully rectify that issue
by [going here](https://github.com/coreybutler/nvm-windows)!

tl;dr:
- Completely uninstall any instance of node/npm on your windows computer
- Grab the latest version of `nvm`
- run `nvm install "latest"`
- run `nvm use <whichever version was installed from the above step>`
- ???
- Profit



## Configuration
Only configuration needed by Doge Bot is the authentication token. 

However, this is per basis of each user. If you are trying to test your changes locally, provide your own credentials inside of the following:
```
# auth/auth.json
{
    "token":    "<insert your Discord Bot token here>",
    "id":       "<insert your Discord Bot id here>"
}
```

You can get more information about how to get your own credentials for the bot [from here](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token).

## Running
To run Doge Bot, execute the following in your favorite terminal/console:
```
npm run bot
```

Provided that you have the credentials and completed the initial installation, the bot should run correctly.